
# Initializer
Import and initialize libraries to be used


```python
# jupyter magic to show graphs in the notebook
%config InlineBackend.rc = {'figure.figsize' : (10, 5) }
%matplotlib inline
import re              # library for regular expressions
import logging         # library to prin interpretable log messages
import numpy as np     # library to process numeric data
import matplotlib.pyplot as plt   # python's answer to plotting
import pandas as pd    # library to read, organize and manipulate data
from fuzzywuzzy import fuzz # string matching library using Levenshtein Distance 
import ipywidgets      # library to use interactive widgets in jupyter notebook
import multiprocessing as mp # library for parallel processing to speed things up
import seaborn as sns
from functools import partial

# import plotly as pltly # convert matplotlib plots to interactive graphs
# pltly.offline.enable_mpl_offline(resize=True, show_link=False) # make interactive graphs render offline

# setup logging configuration
logging.basicConfig(format='%(asctime)s.%(msecs)03d; %(levelname)s; %(funcName)s:%(lineno)d; %(message)s', 
                    level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S')
```

    2018-03-28 17:49:36.933; DEBUG; pylab_setup:90; backend module://ipykernel.pylab.backend_inline version unknown


# Read raw data
Retrieve raw data in CSV to pandas dataframes.

## Attempt manual reading of CSV


```python
with open('data_in/DBLP1.csv', 'rb') as fp:
    line_bytes = fp.readline()
    line_str = line_bytes.decode('utf-8','replace') # this line is able to replace unicode with accented characters
```

## Split the string sequence into individual lines and create pandas dataframe
The first line is the column names. 


```python
lines = line_str.splitlines()
```


```python
line = lines[0]
print(line.split(','))
```

    ['idDBLP', 'title', 'authors', 'venue', 'year', 'Row_ID']


Now we have each line of the CSV containing column values separated by `,` character. We can now use the `.split` operation on the strings to extract all the characters separated by comma. However, the list of authors also comtains commas and cannot be directly applied. There is, however, a pattern in the list of authors which is that the list of authors is usually enclosed within double-quotes (`"`). This list of strings within the double quotes will mean that they be joined together to form a single column. The final check condition is that the number of extracted elements from each row should be equal to the total number of columns in the dataset. This is also true in cases where the long text is enclosed within the double-quotes.


```python
columns = lines[0].split(',')
df = pd.DataFrame(columns=columns)
```


```python
count = 0       # this is for debugging purposes
for line in lines[1:]:    
    count += 1     # this commented block was used to test the loop for small data samples
#     if count > 10:
#         break

    row = line.split(',')   # extract all comma-separated sub-strings
    start_author = False    # marker for start and end of joining the list of authors
    to_remove = []          # list to hold the indexes to be removed from the row
    for i in range(len(row)):   # for each comma sub-string
        if start_author:        # if a list-of authors is found
            row[start_author] = row[start_author] + ',' + row[i] # append the substring to first author
            to_remove.append(i)    # mark this index to be removed
        if row[i][0] == '"':       # if first character is a double-quote
            start_author = i       # mark this index 
        if row[i][-1] == '"':      # if last character is a double-quote
            start_author = False   # mark done-appending authors list
    
    # create new list `row2` to be used to append to dataframe
    if len(to_remove) == 0:      # if nothing to remove
        row2 = row
    else:                        # some elements marked to remove
        row2 = []
        for i in range(len(row)): # create `row2` with accepted index elements 
            if i in to_remove:
                continue
            row2.append(row[i])

    if len(row2) != len(columns):   # sanity check
        logging.error('{}: {} != {}\t {}'.format(count, len(row2), len(columns), row2))
        break
    
#     logging.info(line)
#     logging.info(row)
#     logging.info(row2)
#     logging.info('row length = {}'.format(len(row2)))
#     logging.info('-----------')
    
    d = dict()                                  # create a temporary dictionary element 
    for idx, col in enumerate(columns):         # 
        d[col] = row2[idx]
    df = df.append(d, ignore_index=True)
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idDBLP</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>Row_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>conf/vldb/RusinkiewiczKTWM95</td>
      <td>Towards a Cooperative Transaction Model - The ...</td>
      <td>"M Rusinkiewicz, W Klas, T Tesch, J W�sch, P M...</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>journals/sigmod/EisenbergM02</td>
      <td>SQL/XML is Making Good Progress</td>
      <td>"A Eisenberg, J Melton"</td>
      <td>SIGMOD Record</td>
      <td>2002</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>conf/vldb/AmmannJR95</td>
      <td>Using Formal Methods to Reason about Semantics...</td>
      <td>"P Ammann, S Jajodia, I Ray"</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>journals/sigmod/Liu02</td>
      <td>Editor's Notes</td>
      <td>L Liu</td>
      <td>SIGMOD Record</td>
      <td>2002</td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>journals/sigmod/Hammer02</td>
      <td>Report on the ACM Fourth International Worksho...</td>
      <td>N/A</td>
      <td>N/A</td>
      <td>2002</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
</div>



## Consolidate above method into a helper function
This function will take the CSV file path as an argument, read, extract and parse the data to a pandas dataframe and return it as the output.


```python
def read_csv(file_path):
    # from 2.2 above
    with open(file_path, 'rb') as fp:
        line_bytes = fp.readline()
        line_str = line_bytes.decode('utf-8','replace') # this line is able to replace unicode with accented characters
    
    # from 2.3 above
    lines = line_str.splitlines()
    
    columns = lines[0].split(',')
    df = pd.DataFrame(columns=columns)
    
    count = 0       # this is for debugging purposes
    for line in lines[1:]:    
        count += 1     # this commented block was used to test the loop for small data samples
    #     if count > 10:
    #         break

        row = line.split(',')   # extract all comma-separated sub-strings
        start_author = False    # marker for start and end of joining the list of authors
        to_remove = []          # list to hold the indexes to be removed from the row
        for i in range(len(row)):   # for each comma sub-string
            if start_author:        # if a list-of authors is found
                row[start_author] = row[start_author] + ',' + row[i] # append the substring to first author
                to_remove.append(i)    # mark this index to be removed
            if len(row[i]) == 0:       # sanity check
                continue
            if row[i][0] == '"':       # if first character is a double-quote
                start_author = i       # mark this index 
            if row[i][-1] == '"':      # if last character is a double-quote
                start_author = False   # mark done-appending authors list

        # create new list `row2` to be used to append to dataframe
        if len(to_remove) == 0:      # if nothing to remove
            row2 = row
        else:                        # some elements marked to remove
            row2 = []
            for i in range(len(row)): # create `row2` with accepted index elements 
                if i in to_remove:
                    continue
                row2.append(row[i])

        if len(row2) != len(columns):   # sanity check
            logging.error('{}: {} != {}\t {}'.format(count, len(row2), len(columns), row2))
            break

    #     logging.info(line)
    #     logging.info(row)
    #     logging.info(row2)
    #     logging.info('row length = {}'.format(len(row2)))
    #     logging.info('-----------')

        d = dict()                                  # create a temporary dictionary element 
        for idx, col in enumerate(columns):         # 
            d[col] = row2[idx]
        df = df.append(d, ignore_index=True)

    return df
```


```python
dblp = read_csv('data_in/DBLP1.csv')
scholar = read_csv('data_in/Scholar.csv')
```

    2018-03-28 17:49:51.783; ERROR; read_csv:44; 101: 11 != 6	 ['kI-Hiv-7NZsJ', 'Gastroenteritis due to Kanagawa negative Vibrio parahaemolyticus', '"', ' I Goto', ' I Minematsu', ' N Ikeda', ' N Asano', ' M  "', '"Lancet i,"', '1987', '101']



```python
dblp.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idDBLP</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>Row_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>conf/vldb/RusinkiewiczKTWM95</td>
      <td>Towards a Cooperative Transaction Model - The ...</td>
      <td>"M Rusinkiewicz, W Klas, T Tesch, J W�sch, P M...</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>journals/sigmod/EisenbergM02</td>
      <td>SQL/XML is Making Good Progress</td>
      <td>"A Eisenberg, J Melton"</td>
      <td>SIGMOD Record</td>
      <td>2002</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>conf/vldb/AmmannJR95</td>
      <td>Using Formal Methods to Reason about Semantics...</td>
      <td>"P Ammann, S Jajodia, I Ray"</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>journals/sigmod/Liu02</td>
      <td>Editor's Notes</td>
      <td>L Liu</td>
      <td>SIGMOD Record</td>
      <td>2002</td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>journals/sigmod/Hammer02</td>
      <td>Report on the ACM Fourth International Worksho...</td>
      <td>N/A</td>
      <td>N/A</td>
      <td>2002</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
</div>




```python
scholar.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>aKcZKwvwbQwJ</td>
      <td>11578 Sorrento Valley Road</td>
      <td>QD Inc</td>
      <td>"San Diego,"</td>
      <td></td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ixKfiTHoaDoJ</td>
      <td>Initiation of crazes in polystyrene</td>
      <td>"AS Argon, JG Hannoosh"</td>
      <td>"Phil. Mag,"</td>
      <td></td>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3BxllB4wwcIJ</td>
      <td>Immunogold labelling is a quantitative method ...</td>
      <td>"GH Hansen, LL Wetterberg, H Sj퀌_str퀌_m, O Nor...</td>
      <td>"The Histochemical Journal,"</td>
      <td>1992</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>d2WWxwKMex4J</td>
      <td>The Burden of Infectious Disease Among Inmates...</td>
      <td>"TM Hammett, P Harmon, W Rhodes"</td>
      <td>see</td>
      <td></td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>cZCX-AQpjccJ</td>
      <td>The Role of Faculty Advising in Science and En...</td>
      <td>JR Cogdell</td>
      <td>"NEW DIRECTIONS FOR TEACHING AND LEARNING,"</td>
      <td>1995</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
</div>



## Attempt lazy loading using `pandas.read_csv` function


```python
try: 
    dblp = pd.read_csv('data_in/DBLP1.csv', encoding='utf-8', header=0, )
#     scholar = pd.read_csv('data_in/Scholar.csv')
except Exception as e:
    logging.error(e)
```

    2018-03-28 17:49:51.852; ERROR; <module>:5; 'utf-8' codec can't decode byte 0x92 in position 3: invalid start byte


Note that the above attempt has failed presenting errors in the encoded format of the CSV file. We will now have to open the file and parse individual lines. To do so, I will be reading one line at a time and verify the output using the above approach. Manually reading the CSV file also showed the newline character to be the `\r` i.e., carriage return instead of the universal `\n` or `\r\n`.


```python
try: 
    dblp = pd.read_csv('data_in/DBLP1.csv', encoding='iso8859_15', header=0, engine='python')
    scholar = pd.read_csv('data_in/Scholar.csv', encoding='iso8859_15', engine='python')
except Exception as e:
    logging.error(e)
```


```python
dblp.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idDBLP</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>Row_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>conf/vldb/RusinkiewiczKTWM95</td>
      <td>Towards a Cooperative Transaction Model - The ...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>journals/sigmod/EisenbergM02</td>
      <td>SQL/XML is Making Good Progress</td>
      <td>A Eisenberg, J Melton</td>
      <td>SIGMOD Record</td>
      <td>2002</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>conf/vldb/AmmannJR95</td>
      <td>Using Formal Methods to Reason about Semantics...</td>
      <td>P Ammann, S Jajodia, I Ray</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>journals/sigmod/Liu02</td>
      <td>Editor's Notes</td>
      <td>L Liu</td>
      <td>SIGMOD Record</td>
      <td>2002</td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>journals/sigmod/Hammer02</td>
      <td>Report on the ACM Fourth International Worksho...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2002</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
</div>




```python
scholar.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>aKcZKwvwbQwJ</td>
      <td>11578 Sorrento Valley Road</td>
      <td>QD Inc</td>
      <td>San Diego,</td>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ixKfiTHoaDoJ</td>
      <td>Initiation of crazes in polystyrene</td>
      <td>AS Argon, JG Hannoosh</td>
      <td>Phil. Mag,</td>
      <td>NaN</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3BxllB4wwcIJ</td>
      <td>Immunogold labelling is a quantitative method ...</td>
      <td>GH Hansen, LL Wetterberg, H Sjí_strí_m, O ...</td>
      <td>The Histochemical Journal,</td>
      <td>1992.0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>d2WWxwKMex4J</td>
      <td>The Burden of Infectious Disease Among Inmates...</td>
      <td>TM Hammett, P Harmon, W Rhodes</td>
      <td>see</td>
      <td>NaN</td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>cZCX-AQpjccJ</td>
      <td>The Role of Faculty Advising in Science and En...</td>
      <td>JR Cogdell</td>
      <td>NEW DIRECTIONS FOR TEACHING AND LEARNING,</td>
      <td>1995.0</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
</div>



Notice that this approach using pandas is much faster but there are inconsistencies in the Unicode text between the two approaches.
We can proceed using either approaches.

# Finding matches
We will do a two-pass approach. In each pass, we iterate through the rows of dataframe and find any match in the other dataframe. The first approximation for a match is the `title` field. Note the inconsistencies in text cases. for synchronization, we will create a lower-case column for comparison.


```python
# example
display(dblp.head(1))
ex_title = dblp.loc[0,'title']    # get title of first row in dblp
print('**Searching for title:** ' + ex_title)
scholar.loc[scholar.title.str.contains(ex_title, case=False)]  # search for that title in scholar
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idDBLP</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>Row_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>conf/vldb/RusinkiewiczKTWM95</td>
      <td>Towards a Cooperative Transaction Model - The ...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth</td>
      <td>VLDB</td>
      <td>1995</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>


    **Searching for title:** Towards a Cooperative Transaction Model - The Cooperative Activity Model





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



This has returned **0** results. However, if we search for only parts of the text as below:


```python
scholar.loc[scholar.title.str.contains('Towards a Cooperative Transaction Model', case=False)]  # search for that title in scholar
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>46546</th>
      <td>T2fm7Wb1ak4J</td>
      <td>Towards a Cooperative Transaction Model-The Co...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Waesch, P Muth</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>46547</td>
    </tr>
    <tr>
      <th>59993</th>
      <td>iWNLOYCQX-YJ</td>
      <td>W asch, J., Muth, P.: Towards a cooperative tr...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch</td>
      <td>Proc. ofthe 21 stInternational Conference on V...</td>
      <td>NaN</td>
      <td>59994</td>
    </tr>
  </tbody>
</table>
</div>



It is very clear to see that there are a lot of inconsistencies. The obvious one here being that in scholar database, there are no spaces before and after the hyphen `-` in the title leading to no results before. We can address this by manually ensuring that is exactly one space before and exactly one space after the hyphen. However, this manual approach may not address all the issues in the unclean data. 

Since the objective here is to find matches between the two dataframes, we can check the probabilities of matches.

## Approach 1 - Probability of matching words
This is a fairly straight forward approach in which individual words are extracted from the title to be searched. Words that are two characters or smaller are rejected. The remaining words are used to compute the probability of existance in the other dataframe. The probability is computed as:
$$P = \frac{\text{number of matches observed}}{\text{number of words searched}}$$
Since all the words should match, we will set a threshold of `1.0` to accept the matches


```python
words = dblp.title[0].split(' ')
print(words)
words = [w for w in words if len(w) >= 3]
print(words)

p = scholar.title.str.count('|'.join(words), re.IGNORECASE) / len(words)
fig, ax = plt.subplots(1, 1)
ax.plot(scholar.ROW_ID.T, p)
ax.minorticks_on()
ax.grid(True, which='both')
ax.set_xlabel('Scholar Row_ID')
ax.set_ylabel('Match probability')
ax.set_ylim([.5,1])
print('Matches found at:')
display(scholar.loc[p==1,:])
```

    ['Towards', 'a', 'Cooperative', 'Transaction', 'Model', '-', 'The', 'Cooperative', 'Activity', 'Model']
    ['Towards', 'Cooperative', 'Transaction', 'Model', 'The', 'Cooperative', 'Activity', 'Model']
    Matches found at:



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>46546</th>
      <td>T2fm7Wb1ak4J</td>
      <td>Towards a Cooperative Transaction Model-The Co...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Waesch, P Muth</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>46547</td>
    </tr>
    <tr>
      <th>59993</th>
      <td>iWNLOYCQX-YJ</td>
      <td>W asch, J., Muth, P.: Towards a cooperative tr...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch</td>
      <td>Proc. ofthe 21 stInternational Conference on V...</td>
      <td>NaN</td>
      <td>59994</td>
    </tr>
  </tbody>
</table>
</div>


    2018-03-28 17:49:53.532; DEBUG; findfont:1343; findfont: Matching :family=sans-serif:style=normal:variant=normal:weight=normal:stretch=normal:size=10.0 to DejaVu Sans ('/Users/kvedala/miniconda3/lib/python3.6/site-packages/matplotlib/mpl-data/fonts/ttf/DejaVuSans.ttf') with score of 0.050000



![png](output_30_3.png)


From the above graphs, it is clear that the results from this approach match with the manual string search above. Let us try manually a couple more


```python
def approach1(df1, df2, idx=0):
    words = df1.title[idx].split(' ')
    words = [w for w in words if len(w) >= 3]
    
    p = df2.title.str.count('|'.join(words), re.IGNORECASE) / len(words)
    
    return df2.loc[p==1,:]
```


```python
for i in range(1,dblp.shape[0]):
    result = approach1(dblp, scholar, i)
    if result.shape[0] > 1:
        break
print('i = %d' % i)
display(result)
```

    i = 3



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2222</th>
      <td>ntqMqfgRXM4J</td>
      <td>Editor's Notes</td>
      <td>R Goldstein</td>
      <td>The American Statistician,</td>
      <td>1996.0</td>
      <td>2223</td>
    </tr>
    <tr>
      <th>5264</th>
      <td>url:http://www.roc.noaa.gov/news/vol1is4.pdf</td>
      <td>Editor's notes</td>
      <td>NL Smith</td>
      <td>New Directions for Program Evaluation,</td>
      <td>1981.0</td>
      <td>5265</td>
    </tr>
    <tr>
      <th>6327</th>
      <td>TUOBVMb4PBsJ</td>
      <td>Editor's Notes</td>
      <td>DW Leslie</td>
      <td>New Directions for Higher Education,</td>
      <td>NaN</td>
      <td>6328</td>
    </tr>
    <tr>
      <th>16291</th>
      <td>vgcXH5aVpdgJ</td>
      <td>Editor's notes</td>
      <td>TW Banta</td>
      <td>Implementing outcomes assessment: Promise and ...</td>
      <td>NaN</td>
      <td>16292</td>
    </tr>
    <tr>
      <th>22821</th>
      <td>_SsvlmhsusEJ</td>
      <td>Editor's Notes</td>
      <td>JA Neff</td>
      <td>Journal of Emergency</td>
      <td>NaN</td>
      <td>22822</td>
    </tr>
    <tr>
      <th>25454</th>
      <td>url:http://doi.wiley.com/10.1002/tl.7700</td>
      <td>Editor's Notes</td>
      <td>SM Richardson</td>
      <td>New Directions for Teaching and Learning,</td>
      <td>1999.0</td>
      <td>25455</td>
    </tr>
    <tr>
      <th>29706</th>
      <td>skOc65LTInYJ</td>
      <td>Editor's notes</td>
      <td>R Diem</td>
      <td>Social Studies and the Young Learner,</td>
      <td>NaN</td>
      <td>29707</td>
    </tr>
    <tr>
      <th>31205</th>
      <td>xgdBJbatYcIJ</td>
      <td>Editor's notes</td>
      <td>WMK Trochim</td>
      <td>New Directions for Program Evaluation,</td>
      <td>NaN</td>
      <td>31206</td>
    </tr>
    <tr>
      <th>33893</th>
      <td>p0QHT_9_NSkJ</td>
      <td>Editor's notes</td>
      <td>DJ Rog, D Fournier</td>
      <td>New Directions for Program Evaluation,</td>
      <td>NaN</td>
      <td>33894</td>
    </tr>
    <tr>
      <th>36301</th>
      <td>hAuEDElSa2MJ</td>
      <td>Notes on Formalizing Context (Expanded Notes)</td>
      <td>J McCarthy, S Buvac</td>
      <td>Technical Report CS-TN-94-13, Stanford Univers...</td>
      <td>NaN</td>
      <td>36302</td>
    </tr>
    <tr>
      <th>38666</th>
      <td>zMW_rq3S6VgJ</td>
      <td>Editor's Notes</td>
      <td>TE Cyrs</td>
      <td>New Directions for Teaching and Learning,</td>
      <td>NaN</td>
      <td>38667</td>
    </tr>
    <tr>
      <th>50884</th>
      <td>dO8czlfdENQJ</td>
      <td>Editor's notes</td>
      <td>RH Silkman</td>
      <td>New Directions for Program Evaluation,</td>
      <td>NaN</td>
      <td>50885</td>
    </tr>
    <tr>
      <th>53050</th>
      <td>_cil6uzFNtsJ</td>
      <td>EDITOR'S NOTES</td>
      <td>SB Merriam</td>
      <td>New Directions for Adult and Continuing Educat...</td>
      <td>NaN</td>
      <td>53051</td>
    </tr>
    <tr>
      <th>61808</th>
      <td>url:http://doi.wiley.com/10.1002/ev.1658</td>
      <td>Editor's notes</td>
      <td>L Sechrest</td>
      <td>New Directions for Program Evaluation,</td>
      <td>1993.0</td>
      <td>61809</td>
    </tr>
  </tbody>
</table>
</div>


This result is obviously not expected. This means that an additional filter is required - maybe authors. Let us update the function


```python
idx = 3
words = dblp.title[idx].split(' ')
print(words)
words = [w for w in words if len(w) >= 3]
print(words)
p1 = scholar.title.str.count('|'.join(words), re.IGNORECASE) / len(words)

authors = dblp.authors[idx].split(' ')
print(authors)
authors = [w for w in authors if len(w) >= 3]
print(authors)
p2 = scholar.authors.str.count('|'.join(words), re.IGNORECASE) / len(words)

p3 = (p1 + p2) * 0.5

fig, ax = plt.subplots(1, 1)
ax.plot(scholar.ROW_ID.T, p1, scholar.ROW_ID.T, p2, scholar.ROW_ID.T, p3)
ax.minorticks_on()
ax.grid(True, which='both')
ax.set_xlabel('Scholar Row_ID')
ax.set_ylabel('Match probability')
# ax.set_ylim([.5,1])
# print('Matches found at:')
# display(scholar.loc[p==1,:])
```

    ["Editor's", 'Notes']
    ["Editor's", 'Notes']
    ['L', 'Liu']
    ['Liu']





    Text(0,0.5,'Match probability')




![png](output_35_2.png)


We thus see that `p2` and hence `p3` are much less and result in **0** matches. This is true and approach needs to be validated. We will retest the algorithm with the first row of `dblp` dataframe


```python
idx = 0
display(dblp.loc[idx,:])
words = dblp.title[idx].split(' ')
print(words)
words = [w for w in words if len(w) >= 3]
print(words)
p1 = scholar.title.str.count('|'.join(words), re.IGNORECASE) / len(words)

try:
    authors1 = dblp.authors[idx].split(',')
except Exception as e:
    logging.warning('Unable to get authors list in df2\n{}'.format(e))
    del p2
else:
    authors = authors1 #[]
#     for a in authors1:
#         authors += a.split(',')
    print(authors)
    authors = [w for w in authors if len(w) >= 3]
    print(authors)
    p2 = scholar.authors.str.count('|'.join(authors), re.IGNORECASE | re.ASCII) / len(authors)

year = dblp.year[idx]
p3 = (scholar.year == year).values * 0.33

if 'p2' in locals():
    p3 += (p1 + p2) * 0.5
else:
    p3 += p1

fig, ax = plt.subplots(1, 1)
ax.plot(scholar.ROW_ID.T, p1, label='p1')
ax.plot(scholar.ROW_ID.T, p2, label='p2')
ax.plot(scholar.ROW_ID.T, p3, label='p3')
ax.minorticks_on()
ax.grid(True, which='both')
ax.set_xlabel('Scholar Row_ID')
ax.set_ylabel('Match probability')
ax.set_ylim([.5,1])
# print('Matches found at:')
display(scholar.loc[(p3 >= .8),:])
```


    idDBLP                          conf/vldb/RusinkiewiczKTWM95
    title      Towards a Cooperative Transaction Model - The ...
    authors     M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth
    venue                                                   VLDB
    year                                                    1995
    Row_ID                                                     1
    Name: 0, dtype: object


    ['Towards', 'a', 'Cooperative', 'Transaction', 'Model', '-', 'The', 'Cooperative', 'Activity', 'Model']
    ['Towards', 'Cooperative', 'Transaction', 'Model', 'The', 'Cooperative', 'Activity', 'Model']
    ['M Rusinkiewicz', ' W Klas', ' T Tesch', ' J W\x8asch', ' P Muth']
    ['M Rusinkiewicz', ' W Klas', ' T Tesch', ' J W\x8asch', ' P Muth']



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>10224</th>
      <td>019zSr3Lx4EJ</td>
      <td>Towards a cooperative activity model-the coope...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Wasch, P Muth</td>
      <td>Proceedings of the 21st International Conferen...</td>
      <td>NaN</td>
      <td>10225</td>
    </tr>
    <tr>
      <th>46546</th>
      <td>T2fm7Wb1ak4J</td>
      <td>Towards a Cooperative Transaction Model-The Co...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Waesch, P Muth</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>46547</td>
    </tr>
    <tr>
      <th>59993</th>
      <td>iWNLOYCQX-YJ</td>
      <td>W asch, J., Muth, P.: Towards a cooperative tr...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch</td>
      <td>Proc. ofthe 21 stInternational Conference on V...</td>
      <td>NaN</td>
      <td>59994</td>
    </tr>
  </tbody>
</table>
</div>



![png](output_37_3.png)


From this graph, we can make the following observations:

1. combination required us to loosen the acceptance threshold to 80%
2. doing so revealed two new matches
3. only of the two new matches had acceptable threshold

The matching algorithm failed for the fourth match because of spelling errors in the names. We will also include year comparison in the search.


```python
def approach1(df1, df2, idx=0):
    words = df1.title[idx].split(' ')
    words = [w for w in words if len(w) >= 3]
    
    p1 = df2.title.str.count('|'.join(words), re.IGNORECASE) / len(words)
    
    try:
        authors = df1.authors[idx].split(',')
    except Exception as e:
        logging.warning('Unable to get authors list in df2\n{}'.format(e))
    else:
#         authors = []
#         for a in authors1:
#             authors += a.split(',')
#         authors = [w for w in authors if len(w) >= 3]
        p2 = df2.authors.str.count('|'.join(authors), re.IGNORECASE) / len(authors)
        
    year = df1.year[idx]
    p3 = (df2.year == year).values * 0.33

    if 'p2' in locals():
        p3 += (p1 + p2) * 0.5
    else:
        p3 += p1
    
    return df2.loc[(p3 >= .8),:]
```


```python
for i in range(1,5):#dblp.shape[0]):
    print('i = %d' % i)
    result = approach1(dblp, scholar, i)
#     if result.shape[0] > 0:
#         break
    if result.shape[0] == 0:
        continue
    display(dblp.loc[i])
    display(result)
```

    i = 1



    idDBLP        journals/sigmod/EisenbergM02
    title      SQL/XML is Making Good Progress
    authors              A Eisenberg, J Melton
    venue                        SIGMOD Record
    year                                  2002
    Row_ID                                   2
    Name: 1, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>23641</th>
      <td>6uelfg3RgEoJ</td>
      <td>An Early Look at XQuery</td>
      <td>A Eisenberg, J Melton</td>
      <td>SIGMOD Record,</td>
      <td>2002.0</td>
      <td>23642</td>
    </tr>
    <tr>
      <th>63689</th>
      <td>wgK6p4mDSIMJ</td>
      <td>SQL/XML is Making Good Progress</td>
      <td>A Eisenberg, J Melton</td>
      <td>SIGMOD Record,</td>
      <td>2002.0</td>
      <td>63690</td>
    </tr>
  </tbody>
</table>
</div>


    i = 2



    idDBLP                                  conf/vldb/AmmannJR95
    title      Using Formal Methods to Reason about Semantics...
    authors                           P Ammann, S Jajodia, I Ray
    venue                                                   VLDB
    year                                                    1995
    Row_ID                                                     3
    Name: 2, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>21170</th>
      <td>x-H7BqZ0Hw8J</td>
      <td>Using Formal Methods to Reason about Semantics...</td>
      <td>P Ammann, S Jajodia, I Ray</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>21171</td>
    </tr>
  </tbody>
</table>
</div>


    i = 3
    i = 4


    2018-03-28 17:49:59.750; WARNING; approach1:10; Unable to get authors list in df2
    'float' object has no attribute 'split'



    idDBLP                              journals/sigmod/Hammer02
    title      Report on the ACM Fourth International Worksho...
    authors                                                  NaN
    venue                                                    NaN
    year                                                    2002
    Row_ID                                                     5
    Name: 4, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4974</th>
      <td>url:http://portal.acm.org/ft_gateway.cfm%3Fid%...</td>
      <td>Report on the ACM Fourth International Worksho...</td>
      <td>J Hammer</td>
      <td>SIGMOD Record,</td>
      <td>2002.0</td>
      <td>4975</td>
    </tr>
  </tbody>
</table>
</div>


This approach clearly works great but not quite perfect. In order to have word play, we must implement Natural Language Processing (NLP) along with the above insights.

## Approach 2 - Approach 1 + NLP
We will use the simpler NLP interface provided by the package `fuzzywuzzy`. 


```python
idx = 0
display(dblp.loc[idx,:])
title = dblp.title.values[idx]
p1 = scholar.title.apply(lambda t: fuzz.ratio(t, title)).values

author = dblp.authors.values[idx]
venue = dblp.venue.values[idx]

p2, p3 = [], []
for _, row in scholar.iterrows():
    try:
        p = fuzz.partial_token_sort_ratio(row['authors'], author)
    except Exception as e:
        p2.append(0)
    else:
        p2.append(p)

    try:
        p = fuzz.ratio(row['venue'], venue)
    except Exception as e:
        p3.append(0)
    else:
        p3.append(p)

p2 = np.array(p2)
p3 = np.array(p3)

year = dblp.year[idx]
p4 = (scholar.year == year).values * 1.0

p1 = p1 * 0.75
p2 = p2 * 0.10
p3 = p3 * 0.05
p4 = p4 * 0.10

p = p1 + p2 + p3 + p4

fig, ax = plt.subplots(1, 1)
ax.plot(scholar.ROW_ID.T, p1, label='p1')
ax.plot(scholar.ROW_ID.T, p2, label='p2')
ax.plot(scholar.ROW_ID.T, p3, label='p3')
ax.plot(scholar.ROW_ID.T, p4, label='p4')
ax.plot(scholar.ROW_ID.T, p, label='p')
ax.minorticks_on()
ax.grid(True, which='both')
ax.set_xlabel('Scholar Row_ID')
ax.set_ylabel('Match probability')
ax.legend()
ax.set_ylim([50,100])
# print('Matches found at:')
display(scholar.loc[(p >= 80),:])
```


    idDBLP                          conf/vldb/RusinkiewiczKTWM95
    title      Towards a Cooperative Transaction Model - The ...
    authors     M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth
    venue                                                   VLDB
    year                                                    1995
    Row_ID                                                     1
    Name: 0, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>46546</th>
      <td>T2fm7Wb1ak4J</td>
      <td>Towards a Cooperative Transaction Model-The Co...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Waesch, P Muth</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>46547</td>
    </tr>
  </tbody>
</table>
</div>



![png](output_43_2.png)



```python
def approach2(df1, df2, idx=0):
    title = df1.title.values[idx]
    p1 = df2.title.apply(lambda t: fuzz.partial_ratio(t, title))

    author = df1.authors.values[idx]
    venue = df1.venue.values[idx]

    p2, p3 = [], []
    for _, row in df2.iterrows():
        try:
            p = fuzz.partial_ratio(row['authors'], author)
        except Exception as e:
            p2.append(0)
        else:
            p2.append(p)
        
        try:
            p = fuzz.partial_ratio(row['venue'], venue)
        except Exception as e:
            p3.append(0)
        else:
            p3.append(p)

    p2 = np.array(p2)
    p3 = np.array(p3)

    year = df1.year[idx]
    p4 = (scholar.year == year).values * 1.0

    p1 = p1 * 0.80
    p2 = p2 * 0.10
    p3 = p3 * 0.05
    p4 = p4 * 0.05

    p = p1 + p2 + p3 + p4

    return df2.loc[(p >= 80),:]
```


```python
for i in range(5):#dblp.shape[0]):
    print('i = %d' % i)
    result = approach2(dblp, scholar, i)
#     if result.shape[0] > 0:
#         break
    if result.shape[0] == 0:
        continue
    display(dblp.loc[i])
    display(result)
```

    i = 0



    idDBLP                          conf/vldb/RusinkiewiczKTWM95
    title      Towards a Cooperative Transaction Model - The ...
    authors     M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth
    venue                                                   VLDB
    year                                                    1995
    Row_ID                                                     1
    Name: 0, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>12335</th>
      <td>Z_g8bBtjKygJ</td>
      <td></td>
      <td>M Barr</td>
      <td>NaN</td>
      <td>1979.0</td>
      <td>12336</td>
    </tr>
    <tr>
      <th>46546</th>
      <td>T2fm7Wb1ak4J</td>
      <td>Towards a Cooperative Transaction Model-The Co...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Waesch, P Muth</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>46547</td>
    </tr>
    <tr>
      <th>59993</th>
      <td>iWNLOYCQX-YJ</td>
      <td>W asch, J., Muth, P.: Towards a cooperative tr...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch</td>
      <td>Proc. ofthe 21 stInternational Conference on V...</td>
      <td>NaN</td>
      <td>59994</td>
    </tr>
  </tbody>
</table>
</div>


    i = 1



    idDBLP        journals/sigmod/EisenbergM02
    title      SQL/XML is Making Good Progress
    authors              A Eisenberg, J Melton
    venue                        SIGMOD Record
    year                                  2002
    Row_ID                                   2
    Name: 1, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>12335</th>
      <td>Z_g8bBtjKygJ</td>
      <td></td>
      <td>M Barr</td>
      <td>NaN</td>
      <td>1979.0</td>
      <td>12336</td>
    </tr>
    <tr>
      <th>56489</th>
      <td>MY3eoV4BuDIJ</td>
      <td>Good</td>
      <td>RF Goodwin, W James</td>
      <td>&amp;hellip;  Two Small Communities Waterfronts: A...</td>
      <td>NaN</td>
      <td>56490</td>
    </tr>
    <tr>
      <th>63689</th>
      <td>wgK6p4mDSIMJ</td>
      <td>SQL/XML is Making Good Progress</td>
      <td>A Eisenberg, J Melton</td>
      <td>SIGMOD Record,</td>
      <td>2002.0</td>
      <td>63690</td>
    </tr>
  </tbody>
</table>
</div>


    i = 2



    idDBLP                                  conf/vldb/AmmannJR95
    title      Using Formal Methods to Reason about Semantics...
    authors                           P Ammann, S Jajodia, I Ray
    venue                                                   VLDB
    year                                                    1995
    Row_ID                                                     3
    Name: 2, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>12335</th>
      <td>Z_g8bBtjKygJ</td>
      <td></td>
      <td>M Barr</td>
      <td>NaN</td>
      <td>1979.0</td>
      <td>12336</td>
    </tr>
    <tr>
      <th>21170</th>
      <td>x-H7BqZ0Hw8J</td>
      <td>Using Formal Methods to Reason about Semantics...</td>
      <td>P Ammann, S Jajodia, I Ray</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>21171</td>
    </tr>
  </tbody>
</table>
</div>


    i = 3



    idDBLP     journals/sigmod/Liu02
    title             Editor's Notes
    authors                    L Liu
    venue              SIGMOD Record
    year                        2002
    Row_ID                         4
    Name: 3, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2222</th>
      <td>ntqMqfgRXM4J</td>
      <td>Editor's Notes</td>
      <td>R Goldstein</td>
      <td>The American Statistician,</td>
      <td>1996.0</td>
      <td>2223</td>
    </tr>
    <tr>
      <th>5264</th>
      <td>url:http://www.roc.noaa.gov/news/vol1is4.pdf</td>
      <td>Editor's notes</td>
      <td>NL Smith</td>
      <td>New Directions for Program Evaluation,</td>
      <td>1981.0</td>
      <td>5265</td>
    </tr>
    <tr>
      <th>6327</th>
      <td>TUOBVMb4PBsJ</td>
      <td>Editor's Notes</td>
      <td>DW Leslie</td>
      <td>New Directions for Higher Education,</td>
      <td>NaN</td>
      <td>6328</td>
    </tr>
    <tr>
      <th>12335</th>
      <td>Z_g8bBtjKygJ</td>
      <td></td>
      <td>M Barr</td>
      <td>NaN</td>
      <td>1979.0</td>
      <td>12336</td>
    </tr>
    <tr>
      <th>22821</th>
      <td>_SsvlmhsusEJ</td>
      <td>Editor's Notes</td>
      <td>JA Neff</td>
      <td>Journal of Emergency</td>
      <td>NaN</td>
      <td>22822</td>
    </tr>
    <tr>
      <th>25454</th>
      <td>url:http://doi.wiley.com/10.1002/tl.7700</td>
      <td>Editor's Notes</td>
      <td>SM Richardson</td>
      <td>New Directions for Teaching and Learning,</td>
      <td>1999.0</td>
      <td>25455</td>
    </tr>
    <tr>
      <th>38666</th>
      <td>zMW_rq3S6VgJ</td>
      <td>Editor's Notes</td>
      <td>TE Cyrs</td>
      <td>New Directions for Teaching and Learning,</td>
      <td>NaN</td>
      <td>38667</td>
    </tr>
  </tbody>
</table>
</div>


    i = 4



    idDBLP                              journals/sigmod/Hammer02
    title      Report on the ACM Fourth International Worksho...
    authors                                                  NaN
    venue                                                    NaN
    year                                                    2002
    Row_ID                                                     5
    Name: 4, dtype: object



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4974</th>
      <td>url:http://portal.acm.org/ft_gateway.cfm%3Fid%...</td>
      <td>Report on the ACM Fourth International Worksho...</td>
      <td>J Hammer</td>
      <td>SIGMOD Record,</td>
      <td>2002.0</td>
      <td>4975</td>
    </tr>
    <tr>
      <th>12335</th>
      <td>Z_g8bBtjKygJ</td>
      <td></td>
      <td>M Barr</td>
      <td>NaN</td>
      <td>1979.0</td>
      <td>12336</td>
    </tr>
  </tbody>
</table>
</div>


## Approach 3 - Modified Approach 2
In approach 2, we notice that the scaling factors for each of the matching feature needs to be arbitrarily tweaked. Instead, in this approach, we will create a new sentence combining the three and find a combined score from the fuzzywuzzy partial matching algorithm.
During testing and verification, it was observed that due to the amount of inconsistencies in the dataset, the standard [*Levenshtein distance*](https://en.wikipedia.org/wiki/Levenshtein_distance) computation is in itself unreliable. *Levenshtein distance* is a metric for differences between two strings or words (aka *tokens*). It was observed that the mean of scores for string matching and token matching provided with the best metric for identifying the true matches and contrasting with the incorrect matches.


```python
idx = 0
display(dblp.loc[idx,:])
title = dblp.title.values[idx]
author = dblp.authors.values[idx]
venue = dblp.venue.values[idx]
year = dblp.year.values[idx]
txt_reference = '{}. {}. {}. {:.0f}.'.format(title, author, venue, year)
print(txt_reference)

def txt_compare(row, txt_ref):
    txt_cmp = '{}. {}. {}. {:.0f}.'.format(row['title'], row['authors'], row['venue'], row['year'])

    try:
        pp = fuzz.token_set_ratio(txt_cmp, txt_ref)
        pp += fuzz.ratio(txt_cmp, txt_ref)
    except Exception as e:
        return 0
    else:
        return pp * 0.5

# p = []
# for _, row in scholar.iterrows():
# #     title = row['title']
# #     author = row['authors']
# #     venue = row['venue']
# #     year = row['year']
#     txt_cmp = '{}. {}. {}. {}.'.format(row['title'], row['authors'], row['venue'], row['year'])

#     try:
#         pp = fuzz.fuzz.partial_ratio(txt_cmp, txt_reference)
#     except Exception as e:
#         p.append(0)
#     else:
#         p.append(pp)

p, idx = [0.0] * scholar.shape[0], 0
with mp.Pool(mp.cpu_count()-1) as pool:
    for pp in pool.imap(
        partial(txt_compare, txt_ref=txt_reference), 
        [row for _, row in scholar.iterrows()],
        chunksize=1000
    ):
        p[idx] = pp
        idx += 1
p = np.array(p)

num_std = 3

print('max: {:.2f}\t std: {:.2f}\t threshold: {:.2f}'.format(p.max(), p.std(), p.max()-p.std()*num_std))

fig, ax = plt.subplots(1, 1)
ax.plot(scholar.ROW_ID.T, p, label='p')
ax.minorticks_on()
ax.grid(True, which='both')
ax.set_xlabel('Scholar Row_ID')
ax.set_ylabel('Match probability')
# ax.legend()
ax.set_ylim([50,100])
# print('Matches found at:')
display(scholar.loc[(p >= (p.max() - (p.std()*num_std))) & (p.max() > 80),:])
```


    idDBLP                          conf/vldb/RusinkiewiczKTWM95
    title      Towards a Cooperative Transaction Model - The ...
    authors     M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth
    venue                                                   VLDB
    year                                                    1995
    Row_ID                                                     1
    Name: 0, dtype: object


    Towards a Cooperative Transaction Model - The Cooperative Activity Model. M Rusinkiewicz, W Klas, T Tesch, J Wsch, P Muth. VLDB. 1995.
    max: 86.50	 std: 3.26	 threshold: 76.71



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idScholar</th>
      <th>title</th>
      <th>authors</th>
      <th>venue</th>
      <th>year</th>
      <th>ROW_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>46546</th>
      <td>T2fm7Wb1ak4J</td>
      <td>Towards a Cooperative Transaction Model-The Co...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch, J Waesch, P Muth</td>
      <td>PROCEEDINGS OF THE INTERNATIONAL CONFERENCE ON...</td>
      <td>1995.0</td>
      <td>46547</td>
    </tr>
    <tr>
      <th>59993</th>
      <td>iWNLOYCQX-YJ</td>
      <td>W asch, J., Muth, P.: Towards a cooperative tr...</td>
      <td>M Rusinkiewicz, W Klas, T Tesch</td>
      <td>Proc. ofthe 21 stInternational Conference on V...</td>
      <td>NaN</td>
      <td>59994</td>
    </tr>
  </tbody>
</table>
</div>



![png](output_47_3.png)



```python
def txt_compare(row, txt_ref):
    """
    Helper function to obtain 
    """
    txt_cmp = '{}. {}. {}. {:.0f}.'.format(row['title'], row['authors'], row['venue'], row['year'])

    try:
        pp = fuzz.token_set_ratio(txt_cmp, txt_ref) 
        pp += fuzz.partial_ratio(txt_cmp, txt_ref)
    except Exception as e:
        return 0
    else:
        return pp * 0.5

def approach3(df1, df2, idx=0, threshold=3, plot=True):
    title = df1.title.values[idx]
    author = df1.authors.values[idx]
    venue = df1.venue.values[idx]
    year = df1.year.values[idx]
    txt_reference = '{}. {}. {}. {:.0f}.'.format(title, author, venue, year)
    if plot:
        print(txt_reference)
    
#     p, idx = [0.0] * df2.shape[0], 0
    with mp.Pool(mp.cpu_count()) as pool:
        p = pool.map(
            partial(txt_compare, txt_ref=txt_reference), 
            [row for _, row in df2.iterrows()],
            chunksize=1000
        )
#         for pp in pool.imap(
#             partial(txt_compare, txt_ref=txt_reference), 
#             [row for _, row in df2.iterrows()],
#             chunksize=500
#         ):
#             p[idx] = pp
#             idx += 1

    p = np.array(p)
    if plot:
        print('max: {:.2f}\t std: {:.2f}\t threshold: {:.2f}'.format(p.max(), p.std(), p.max()-p.std()*threshold))

    if plot:
        fig, ax = plt.subplots(1, 1)
        ax.plot(scholar.ROW_ID.T, p, label='p')
        ax.minorticks_on()
        ax.grid(True, which='both')
        ax.set_xlabel('Scholar Row_ID')
        ax.set_ylabel('Match probability')
    #     ax.legend()
        ax.set_ylim([50,100])

    return df2.loc[(p >= (p.max() - (threshold*p.std()) )) & (p.max() > 80),:]
```


```python
ipywidgets.interact(approach3, df1=ipywidgets.fixed(dblp), df2=ipywidgets.fixed(scholar), idx = range(dblp.shape[0]), threshold=range(1,100))
```


<p>Failed to display Jupyter Widget of type <code>interactive</code>.</p>
<p>
  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean
  that the widgets JavaScript is still loading. If this message persists, it
  likely means that the widgets JavaScript library is either not installed or
  not enabled. See the <a href="https://ipywidgets.readthedocs.io/en/stable/user_install.html">Jupyter
  Widgets Documentation</a> for setup instructions.
</p>
<p>
  If you're reading this message in another frontend (for example, a static
  rendering on GitHub or <a href="https://nbviewer.jupyter.org/">NBViewer</a>),
  it may mean that your frontend doesn't currently support widgets.
</p>






    <function __main__.approach3>



# Create a merged result dataframe


```python
dblp_scholar = pd.DataFrame(columns=['idDBLP', 'idScholar', 'DBLP_Match', 'Scholar_Match', 'Match_ID'])
```


```python
idx = 0
dblp_row = dblp.loc[idx, ['idDBLP', 'Row_ID']]
dblp_row = pd.DataFrame(dblp_row).transpose()
dblp_row.rename(columns={'Row_ID': 'DBLP_Match'}, inplace=True)
%time sch_match = approach3(dblp, scholar, plot=False)
sch_match.drop(columns=['title','authors','venue','year'], inplace=True)
sch_match.rename(columns={'ROW_ID': 'Scholar_Match'}, inplace=True)
```

    /Users/kvedala/miniconda3/lib/python3.6/site-packages/pandas/core/indexing.py:1020: FutureWarning: 
    Passing list-likes to .loc or [] with any missing label will raise
    KeyError in the future, you can use .reindex() as an alternative.
    
    See the documentation here:
    http://pandas.pydata.org/pandas-docs/stable/indexing.html#deprecate-loc-reindex-listlike
      return getattr(section, self.name)[new_key]


    CPU times: user 8.5 s, sys: 314 ms, total: 8.82 s
    Wall time: 29.9 s


    /Users/kvedala/miniconda3/lib/python3.6/site-packages/ipykernel_launcher.py:6: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame
    
    See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
      
    /Users/kvedala/miniconda3/lib/python3.6/site-packages/pandas/core/frame.py:3027: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame
    
    See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
      return super(DataFrame, self).rename(**kwargs)



```python
display(scholar.shape)
for _, sch_match_row in sch_match.iterrows():
    print(sch_match_row)
    print({
        'idDBLP'        : dblp_row['idDBLP'].values[0], 
        'idScholar'     : sch_match_row['idScholar'], 
        'DBLP_Match'    : dblp_row['DBLP_Match'].values[0], 
        'Scholar_Match' : sch_match_row['Scholar_Match'],
        'Match_ID'      : '{}_{}'.format(dblp_row['DBLP_Match'].values[0], sch_match_row['Scholar_Match'])
    })
    dblp_scholar = dblp_scholar.append([{
        'idDBLP'        : dblp_row['idDBLP'].values[0], 
        'idScholar'     : sch_match_row['idScholar'], 
        'DBLP_Match'    : dblp_row['DBLP_Match'].values[0], 
        'Scholar_Match' : sch_match_row['Scholar_Match'],
        'Match_ID'      : '{}_{}'.format(dblp_row['DBLP_Match'].values[0], sch_match_row['Scholar_Match'])
    }], ignore_index=True)
display(scholar.drop(index=sch_match.index).shape)
dblp_scholar
```


    (64260, 6)


    idScholar        T2fm7Wb1ak4J
    Scholar_Match           46547
    Name: 46546, dtype: object
    {'idDBLP': 'conf/vldb/RusinkiewiczKTWM95', 'idScholar': 'T2fm7Wb1ak4J', 'DBLP_Match': 1, 'Scholar_Match': 46547, 'Match_ID': '1_46547'}



    (64259, 6)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>idDBLP</th>
      <th>idScholar</th>
      <th>DBLP_Match</th>
      <th>Scholar_Match</th>
      <th>Match_ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>conf/vldb/RusinkiewiczKTWM95</td>
      <td>T2fm7Wb1ak4J</td>
      <td>1</td>
      <td>46547</td>
      <td>1_46547</td>
    </tr>
  </tbody>
</table>
</div>




```python
dblp_scholar = pd.DataFrame(columns=['idDBLP', 'idScholar', 'DBLP_Match', 'Scholar_Match', 'Match_ID'])

for idx in range(dblp.shape[0]):
    dblp_row = dblp.loc[idx, ['idDBLP', 'Row_ID']]
    dblp_row = pd.DataFrame(dblp_row).transpose()
    dblp_row.rename(columns={'Row_ID': 'DBLP_Match'}, inplace=True)
    
    %time sch_match = approach3(dblp, scholar, idx=idx, plot=False)
    sch_match.drop(columns=['title','authors','venue','year'], inplace=True)
    sch_match.rename(columns={'ROW_ID': 'Scholar_Match'}, inplace=True)
    if sch_match.empty:
        continue

    for sch_idx, sch_match_row in sch_match.iterrows():
        dblp_scholar = dblp_scholar.append([{
            'idDBLP'        : dblp_row['idDBLP'].values[0], 
            'idScholar'     : sch_match_row['idScholar'], 
            'DBLP_Match'    : dblp_row['DBLP_Match'].values[0], 
            'Scholar_Match' : sch_match_row['Scholar_Match'],
            'Match_ID'      : '{}_{}'.format(dblp_row['DBLP_Match'].values[0], sch_match_row['Scholar_Match'])
        }], ignore_index=True)
        scholar.drop(index=[sch_idx], inplace=True)
```


```python
dblp_scholar.to_csv('DBLP_Scholar_perfectMapping_KrishnaVedala.csv', index=False)
```
